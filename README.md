# loadsmart

## Overview

This is the loadsmart package. The purpose of this package is to calculate the best available truck for each cargo using their coordinates to calculate the distance.

The total travelled distance for each cargo/truck pair is the sum of the distances from the truck hometown to the cargo origin, then from the cargo origin to the cargo destination, and lastly from the cargo destination to back to the truck's hometown.

For the simplicity of this algorithm I'm using the coordinates to calculate the straight distance using geodesic method.  
In a real world scenario we would need to fetch the routes as using only coordinates it is not accurate, as an example we might have a river in the between that is being not accounted.

The algorithm works in the csv order. For example I find the best truck for the first cargo and remove it, so the second cargo does not have the previously matched truck available for it, the third cargo uses the closest available truck (all trucks minus the trucks that matched for the first and second cargoes) and so on.

### Requirements
- Python 3
- [virtualenv](https://virtualenv.pypa.io/)
- [virtualenvwrapper](https://virtualenvwrapper.readthedocs.io/en/latest/install.html) (optional)

### Dependencies
Dependencies are listed in [requirements.txt](requirements.txt). Dependencies
are automatically installed during loadsmart installation.

### Installing loadsmart

```bash
# setup virtualenv
python3 -m venv --system-site-packages loadsmart
cd loadsmart
source bin/activate

# clone codebase and install
git clone https://github.com/geopython/loadsmart.git
cd loadsmart
python setup.py build
python setup.py install
```

## Running

```bash
cp loadsmart-config.env local.env
vi local.env # update environment variables accordingly

loadsmart --version
```

### Using the API

```python
# Python API examples go here
```

## Development

### Running Tests

```bash
# install dev requirements
pip install -r requirements.txt

# run tests like this:
python loadsmart/tests/run_tests.py

# or this:
python setup.py test

# measure code coverage
coverage run --source=loadsmart -m unittest loadsmart.tests.run_tests
coverage report -m

# sample cli usage:
python loadsmart/main.py -c loadsmart/tests/data/cargo.csv -t  loadsmart/tests/data/trucks.csv 

```

## Releasing

```bash
python setup.py sdist bdist_wheel --universal
```

### Code Conventions

* [PEP8](https://www.python.org/dev/peps/pep-0008)

### Bugs and Issues

All bugs, enhancements and issues are managed on [BitBucket](https://bitbucket.org/thiagotigaz/loadsmart/src/master/).

## Contact

* Thiago Lima - thiagotigaz@gmail.com
