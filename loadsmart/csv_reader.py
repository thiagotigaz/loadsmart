import csv
import logging

from loadsmart.cargo import Cargo
from loadsmart.constants import PRODUCT, ORIGIN_CITY, ORIGIN_STATE, ORIGIN_LAT, ORIGIN_LNG, DESTINATION_CITY, \
    DESTINATION_STATE, DESTINATION_LAT, DESTINATION_LNG, TRUCK, CITY, STATE, LNG, LAT
from loadsmart.truck import Truck

LOG = logging.getLogger(__name__)


class CsvReader:

    def __init__(self, csv_file):
        self.csv_file = csv_file

    def parse(self):
        reader = csv.DictReader(self.csv_file)
        items = [self.to_object(row) for row in reader]
        self.csv_file.close()
        return items

    # To be overridden on subclasses so that it return the proper object
    def to_object(self, row):
        return row


class CargoCsvReader(CsvReader):

    def to_object(self, row):
        cargo = Cargo(
            row[PRODUCT],
            row[ORIGIN_CITY],
            row[ORIGIN_STATE],
            row[ORIGIN_LAT],
            row[ORIGIN_LNG],
            row[DESTINATION_CITY],
            row[DESTINATION_STATE],
            row[DESTINATION_LAT],
            row[DESTINATION_LNG]
        )
        return cargo


class TruckCsvReader(CsvReader):

    def to_object(self, row):
        truck = Truck(
            row[TRUCK],
            row[CITY],
            row[STATE],
            row[LAT],
            row[LNG],
        )
        return truck
