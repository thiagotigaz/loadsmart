import argparse
import logging

from loadsmart.csv_reader import TruckCsvReader, CargoCsvReader
from loadsmart.route_optimizer import RouteOptimizer

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


def create_argparse():
    parser = argparse.ArgumentParser(description='loadsmart route optimizer')
    group = parser.add_argument_group(title='action')
    group.add_argument('-c', '--cargo-csv',
                       help='Path to csv file containing cargos',
                       type=argparse.FileType('r', encoding='UTF-8'),
                       required=True,
                       metavar="FILE")
    group.add_argument('-t', '--truck-csv',
                       help='Path to csv file containing trucks',
                       type=argparse.FileType('r', encoding='UTF-8'),
                       required=True,
                       metavar="FILE")

    return parser.parse_args()


def main():
    try:
        args = create_argparse()
        logger.info(args)
        cargoes = CargoCsvReader(args.cargo_csv).parse()  # Parse csv lines to list of cargo objects
        trucks = TruckCsvReader(args.truck_csv).parse()  # Parse csv lines to list of truck objects
        cargo_by_route = RouteOptimizer(cargoes, trucks).find_optimal_plan()  # Find best match
        # Print the route for each cargo
        for k, v in cargo_by_route.items():
            logger.info('Cargo - %s', k.product)
            logger.info('Route - Truck: %s, Distance: %f mi', v.truck.truck, v.total_distance_miles)
    except Exception as ex:
        logger.error(ex)
        raise


if __name__ == "__main__":
    main()
