import logging

from geopy.distance import geodesic

logger = logging.getLogger(__name__)


class Route:

    def __init__(self, cargo, truck, total_distance_miles):
        self.cargo = cargo
        self.truck = truck
        self.total_distance_miles = total_distance_miles


class RouteOptimizer:

    def __init__(self, cargoes, trucks):
        self.cargoes = cargoes
        self.trucks = trucks

    # Matches each cargo with the truck that has the closest possible travel distance from hometown to cargo origin,
    # destination and then back to hometown.
    # Speed complexity: O(c * log t), c being number of cargoes and t being the number of trucks being removed from
    # the list.
    def find_optimal_plan(self):
        cargo_by_route = {}
        for cargo in self.cargoes:
            routes = []
            shortest_route = None
            for truck in self.trucks:
                distance_in_miles = self.calculate_travel_distance(cargo, truck)
                routes.append(Route(cargo, truck, distance_in_miles))
                if shortest_route is None or shortest_route.total_distance_miles > distance_in_miles:
                    shortest_route = Route(cargo, truck, distance_in_miles)
            cargo_by_route[cargo] = shortest_route
            self.trucks.remove(shortest_route.truck)


        return cargo_by_route

    # Calculate the total distance between necessary for a truck to delivery a cargo. The total distance is the sum of
    # the distances from the truck home town -> cargo origin -> cargo destination -> truck hometown.
    # The distances are calculated using geopy library and geodisec formula. More details on
    # https://en.wikipedia.org/wiki/Geodesics_on_an_ellipsoid
    @staticmethod
    def calculate_travel_distance(cargo, truck):
        # Coordinate pairs
        truck_hometown = (truck.lat, truck.lng)
        cargo_origin = (cargo.origin_lat, cargo.origin_lng)
        cargo_destination = (cargo.destination_lat, cargo.destination_lng)
        # Distances calc
        hometown_to_origin = geodesic(truck_hometown, cargo_origin).miles
        origin_to_destination = geodesic(cargo_origin, cargo_destination).miles
        destination_to_hometown = geodesic(cargo_destination, truck_hometown).miles
        return hometown_to_origin + origin_to_destination + destination_to_hometown
