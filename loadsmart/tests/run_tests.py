import os
import unittest

from loadsmart.constants import PRODUCT, ORIGIN_CITY, ORIGIN_STATE, ORIGIN_LAT, ORIGIN_LNG, DESTINATION_CITY, \
    DESTINATION_LAT, DESTINATION_LNG
from loadsmart.csv_reader import CsvReader, CargoCsvReader, TruckCsvReader
from loadsmart.route_optimizer import RouteOptimizer

TRUCK_CSV = 'trucks.csv'

CARGO_CSV = 'cargo.csv'

TESTDATA_DIR = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'data')


# Helper function to facilitate absolute test file access.
def get_abspath(filepath):
    return os.path.join(TESTDATA_DIR, filepath)


# convenience function to print out test id and desc.
def msg(test_id, test_description):
    return '{}: {}'.format(test_id, test_description)


# Test suite for Loadsmart package.
class CsvReaderTest(unittest.TestCase):

    # setup test fixtures, etc.
    def setUp(self):
        print(msg(self.id(), self.shortDescription()))

    # return to pristine state.
    def tearDown(self):
        pass

    def test_csv_parse(self):
        # Arrange
        csv_file = open(get_abspath(CARGO_CSV), 'r')
        reader = CsvReader(csv_file)
        # Act
        cargoes = reader.parse()
        # Assert
        self.assertEqual(len(cargoes), 7)
        cargo = cargoes[0]
        self.assertEqual(cargo[PRODUCT], 'Light bulbs')
        self.assertEqual(cargo[ORIGIN_CITY], 'Sikeston')
        self.assertEqual(cargo[ORIGIN_STATE], 'MO')
        self.assertEqual(cargo[ORIGIN_LAT], '36.876719')  # TODO handle as number
        self.assertEqual(cargo[ORIGIN_LNG], '-89.5878579')  # TODO handle as number
        self.assertEqual(cargo[DESTINATION_CITY], 'Grapevine')
        self.assertEqual(cargo[DESTINATION_LAT], '32.9342919')  # TODO handle as number
        self.assertEqual(cargo[DESTINATION_LNG], '-97.0780654')  # TODO handle as number


class CargoCsvReaderTest(unittest.TestCase):

    # setup test fixtures, etc.
    def setUp(self):
        print(msg(self.id(), self.shortDescription()))

    # return to pristine state.
    def tearDown(self):
        pass

    def test_cargo_parse(self):
        # Arrange
        csv_file = open(get_abspath(CARGO_CSV), 'r')
        reader = CargoCsvReader(csv_file)
        # Act
        cargoes = reader.parse()
        # Assert
        self.assertEqual(len(cargoes), 7)
        cargo = cargoes[0]
        self.assertEqual(cargo.product, 'Light bulbs')
        self.assertEqual(cargo.origin_city, 'Sikeston')
        self.assertEqual(cargo.origin_state, 'MO')
        self.assertEqual(cargo.origin_lat, '36.876719')  # TODO handle as number
        self.assertEqual(cargo.origin_lng, '-89.5878579')  # TODO handle as number
        self.assertEqual(cargo.destination_city, 'Grapevine')
        self.assertEqual(cargo.destination_lat, '32.9342919')  # TODO handle as number
        self.assertEqual(cargo.destination_lng, '-97.0780654')  # TODO handle as number


class TruckCsvReaderTest(unittest.TestCase):

    # setup test fixtures, etc.
    def setUp(self):
        print(msg(self.id(), self.shortDescription()))

    # return to pristine state.
    def tearDown(self):
        pass

    def test_truck_parse(self):
        # Arrange
        csv_file = open(get_abspath(TRUCK_CSV), 'r')
        reader = TruckCsvReader(csv_file)
        # Act
        trucks = reader.parse()
        # Assert
        self.assertEqual(len(trucks), 44)
        truck = trucks[0]
        self.assertEqual(truck.truck, 'Hartford Plastics Incartford')
        self.assertEqual(truck.city, 'Florence')
        self.assertEqual(truck.state, 'AL')
        self.assertEqual(truck.lat, '34.79981')  # TODO handle as number
        self.assertEqual(truck.lng, '-87.677251')  # TODO handle as number


class RouteOptimizerTest(unittest.TestCase):

    # setup test fixtures, etc.
    def setUp(self):
        print(msg(self.id(), self.shortDescription()))
        self.cargoes = CargoCsvReader(open(get_abspath(CARGO_CSV), 'r')).parse()
        self.trucks = TruckCsvReader(open(get_abspath(TRUCK_CSV), 'r')).parse()
        self.route_optimizer = RouteOptimizer(self.cargoes.copy(), self.trucks.copy())

    # return to pristine state.
    def tearDown(self):
        pass

    def test_calculate_travel_distance(self):
        # Arrange
        cargo = self.cargoes[0]  # Light bulbs
        truck = self.trucks[22]  # Sidhu Trucking Incarrisburg
        # Act
        # (435.184  + 504.510 + 111.681 Miles)
        travel_distance = self.route_optimizer.calculate_travel_distance(cargo, truck)
        # Assert
        self.assertEqual(cargo.product, 'Light bulbs')
        self.assertEqual(truck.truck, 'Sidhu Trucking Incarrisburg')
        self.assertEqual(round(travel_distance, 6), 1051.374599)
        # can be verified using https://www.cqsrg.org/tools/GCDistance/

    def test_find_optimal_plan(self):
        # Act
        cargo_by_route = self.route_optimizer.find_optimal_plan()
        # Assert
        light_bulbs_route = cargo_by_route[self.cargoes[0]]  # Light bulbs - Sidhu Trucking Incarrisburg
        self.assert_route(light_bulbs_route, self.cargoes[0], self.trucks[22], 1051.374599)

        oranges_route = cargo_by_route[self.cargoes[3]]  # Wood - Edmon'S Unique Furniture
        self.assert_route(oranges_route, self.cargoes[3], self.trucks[0], 1399.961725)

        oranges_route = cargo_by_route[self.cargoes[-1]]  # Oranges - Edmon'S Unique Furniture
        self.assert_route(oranges_route, self.cargoes[-1], self.trucks[23], 369.971709)

    def assert_route(self, route, cargo, truck, total_distance_miles):
        self.assertEqual(round(route.total_distance_miles, 6), total_distance_miles)
        self.assertEqual(route.cargo, cargo)
        self.assertEqual(route.truck, truck)


if __name__ == '__main__':
    unittest.main()
