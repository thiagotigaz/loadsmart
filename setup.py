#!/usr/bin/env python
# coding=utf-8

# python distribute file

import io
import os
import sys

from setuptools import Command, find_packages, setup


class PyTest(Command):
    user_options = []

    def initialize_options(self):
        pass

    def finalize_options(self):
        pass

    def run(self):
        import subprocess
        errno = subprocess.call([sys.executable, 'loadsmart/tests/run_tests.py'])
        raise SystemExit(errno)


class PyCoverage(Command):
    user_options = []

    def initialize_options(self):
        pass

    def finalize_options(self):
        pass

    def run(self):
        import subprocess

        errno = subprocess.call(['coverage', 'run', '--source=loadsmart',
                                 '-m', 'unittest',
                                 'loadsmart.tests.run_tests'])
        errno = subprocess.call(['coverage', 'report', '-m'])
        raise SystemExit(errno)


def read(filename, encoding='utf-8'):
    # read file contents
    full_path = os.path.join(os.path.dirname(__file__), filename)
    with io.open(full_path, encoding=encoding) as fh:
        contents = fh.read().strip()
    return contents


setup(
    name="loadsmart",
    version="0.1.0",
    packages=find_packages(exclude=['loadsmart.tests']),
    install_requires=read('requirements.txt').splitlines(),
    dependency_links=[
        # If your project has dependencies on some internal packages that is
        # not on PyPI, you may list package index url here. Then you can just
        # mention package name and version in requirements.txt file.
    ],
    entry_points={
        'console_scripts': [
            'main = loadsmart.main:main',
        ]
    },
    package_data={
        'loadsmart': ['logger.conf']
    },
    author="Thiago Lima",
    author_email="thiagotigaz@gmail.com",
    maintainer="Thiago Lima",
    maintainer_email="thiagotigaz@gmail.com",
    description="loadsmart route optimizer",
    long_description=open('README.md').read(),
    long_description_content_type='text/markdown',
    license="MIT",
    url="https://loadsmart.com/pypi/loadsmart",
    classifiers=[
        'Development Status :: 1 - Beta',
        'Environment :: Console',
        'Programming Language :: Python :: 3.7.3',
    ],
    cmdclass={'test': PyTest, 'coverage': PyCoverage}

)
